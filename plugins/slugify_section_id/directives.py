from docutils.parsers.rst import directives, Directive
from docutils.parsers.rst.directives.parts import Contents
from docutils import io, nodes
from pelican import readers
from pelican.readers import PelicanHTMLTranslator
from pelican.utils import slugify

class Section(Directive):
    """
    A section create

    <div id="name" class="section">
        <div class="container-fluid part *class>
            <div class="block">
                <h2 class="toc">/ name </h2>
                <blockquote class="content">
                    ...
                </blockquote>
            </div>
        </div>
    </div>

    where class is an option in the directive.

    This directive needs one argument which is the name of the section.
    """
    required_arguments = 1
    optional_arguments = 0
    final_argument_whitespace = True
    option_spec = {
        'class': directives.class_option,
    }
    has_content = True

    def run(self):
        self.assert_has_content()

        # Slugigying section ids. The regexp:
        # 1) removes non-word characters, non spaces, except for the hyphens,
        # 2) removes leading spaces,
        # 3) removes trailing spaces,
        # 4) replaces successive spaces and hyphens by only one hyphen.
        section = nodes.section(ids=[slugify(self.arguments[0],
                                             [('[^\\w\\s-]', ''), ('(?u)\\A\\s*', ''), ('(?u)\\s*\\Z', ''), ('[-\\s]+', '-')])])

        section['class'] = self.options.get('class', [])
        section += nodes.title(self.arguments[0], self.arguments[0],
                               classes=['toc'], refid=section['ids'][0])
        blockquote = nodes.block_quote(classes=['content'])
        section += blockquote
        self.state.nested_parse(self.content, self.content_offset, blockquote)
        s = section
        return [s]


class ContentsHook(Contents):
    """ Custom table of contents options """

    def run(self):
        # To avoid automatic 'id*' section backlinks generation (see #128)
        self.options['backlinks'] = Contents.backlinks('none')
        return super().run()


# Extend default translator
class myHTMLTranslator(PelicanHTMLTranslator):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, *kwargs)

    def depart_section(self, node):
        if node.hasattr('class') and self.section_level == 1:
            self.body.append('</div>\n')
            self.body.append('</div>\n')
        super(myHTMLTranslator, self).depart_section(node)

    def visit_section(self, node):
        super(myHTMLTranslator, self).visit_section(node)
        if node.hasattr('class') and self.section_level == 1:
            self.body.append(
                '<div class="container-fluid part {}">\n'.format(' '.join(node['class'])))
            self.body.append('<div class="block">\n')


# Overwrite default translator so that extensions can be chained
readers.PelicanHTMLTranslator = myHTMLTranslator

def register():
    directives.register_directive('section', Section)
    directives.register_directive('contents', ContentsHook)
