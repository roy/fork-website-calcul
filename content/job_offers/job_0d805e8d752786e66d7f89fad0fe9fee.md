Title: CDD niveau IR expert en calcul scientifique à l&#39;Observatoire de Paris (HPC, méthode numériques)
Date: 2021-01-26 07:58
Slug: job_0d805e8d752786e66d7f89fad0fe9fee
Category: job
Authors: Benoit Semelin
Email: benoit.semelin@obspm.fr
Job_Type: CDD
Tags: cdd
Template: job_offer
Job_Location: Observatoire de Paris-Meudon,
Job_Duration: 12 mois, éventuellement renouvelable
Job_Website: https://lerma.obspm.fr/spip.php?article436
Job_Employer: Observatoire de Paris
Expiration_Date: 2021-04-20
Attachment: 

Description des missions :

L’agent participera, en collaboration avec les chercheurs du LERMA, au développement, à l’optimisation et au portage sur des architectures HPC des nouvelles générations de codes numériques indispensables pour répondre aux grands enjeux de la discipline dans les thématiques prioritaires du LERMA (galaxies et grandes structures, milieu interstellaire et plasmas astrophysiques, calculs de physique quantique).
Il consacrera généralement la moitié de son temps aux activités liées à la préparation à SKA.
Par son expertise, il contribuera à amener les prototypes de codes développés dans ce cadre à un niveau de maturité permettant une mise en production en mode service dans les futurs centres de traitement des données SKA (SDP et SKA regional centers).
Il participera en parallèle au développement des codes de modélisation astrophysique au LERMA.

Activités principales :

Réaliser des benchmarks et optimiser des codes des chaînes de traitement des données SKA
Optimiser les codes de modélisation afin que ces derniers puissent tirer parti des nouvelles infrastructures dont celles de calcul HPC
Définir et coordonner la mise en œuvre des solutions techniques pour le traitement et la gestion de grands volumes de données issus des observations ou produits par les codes
Conseiller, orienter et aider les chercheurs du LERMA pour le développement de nouveaux codes sur le choix d’algorithmes et d’outils pour la résolution de leurs problèmes
Assurer une veille technologique sur l’évolution des architectures matérielles et des systèmes de calcul haute performance, des bibliothèques de calcul scientifique, des algorithmes associés.

Perspecives:
Un concours sur poste permanent sur le même profil devrait être ouvert au LERMA à une échelle de temps de 1 à 3 ans.