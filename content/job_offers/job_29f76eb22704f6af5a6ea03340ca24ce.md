Title: Ingénieur Intelligence Artificielle H/F sur le supercalculateur Jean Zay de l&#39;IDRIS
Date: 2020-12-04 13:10
Slug: job_29f76eb22704f6af5a6ea03340ca24ce
Category: job
Authors: Geneviève Morvan
Email: communication@idris.fr
Job_Type: CDD
Tags: cdd
Template: job_offer
Job_Location: Orsay (91)
Job_Duration: 2 ans
Job_Website: http://www.idris.fr
Job_Employer: CNRS-IDRIS
Expiration_Date: 2021-02-26
Attachment: 

L&#39;IDRIS recrute un

**ingénieur Intelligence Artificielle H/F**

pour renforcer l&#39;équipe de support intelligence artificielle sur le supercalculateur Jean Zay

**Missions**

En tant qu&#39;ingénieur spécialiste en Intelligence Artificielle, vous identifiez et évaluez les besoins des utilisateurs de la communauté IA (plus de 400 projets IA à ce jour) sur la plateforme nationale Jean Zay (28 Pflop/s, 2696 GPU V100).

Vous conseillez et accompagnez ces utilisateurs, avec comme optique l&#39;optimisation de l&#39;utilisation des ressources du centre. Vous mettez notamment à disposition votre connaissance de la programmation en Python et votre expertise dans le déploiement des couches logicielles utilisées dans l&#39;IA (TensorFlow et PyTorch).

Vous assurez une veille technologique sur l&#39;évolution des logiciels, méthodes et architectures matérielles en intelligence artificielle pour augmenter l&#39;efficacité de la plate-forme.

Vous formez enfin les utilisateurs à l&#39;usage de la plate-forme et participez à la facilitation de l&#39;accès, travaillant en étroite relation avec les équipes de HPC de l&#39;IDRIS.

**Profil recherché**

- bonne connaissance ou au moins une expérience dans le déploiement de couches logicielles spécifiques IA (TensorFlow, Pytorch),
- maitrise de la programmation Python,
- habitude d&#39;utiliser les outils collaboratifs pour développer des codes applicatifs.
- maîtrise de l&#39;utilisation des environnements, méthodologies et algorithmes pour l&#39;intelligence artificielle et/ou le traitement de données à grande échelle,
- connaissance des architectures de calcul HPC, en particulier des GPU.
- La maîtrise de l&#39;anglais est obligatoire.

Pour plus d&#39;information et /ou pour postuler, consulter le site web de l&#39;IDRIS : 
<http://www.idris.fr/annonces/idris-recrute.html>