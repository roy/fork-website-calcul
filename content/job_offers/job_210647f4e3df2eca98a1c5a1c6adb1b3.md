Title:  Assistant en gestion administrative (H/F)
Date: 2020-06-08 15:35
Slug: job_210647f4e3df2eca98a1c5a1c6adb1b3
Category: job
Authors: Geneviève Morvan
Email: communication@idris.fr
Job_Type: Concours
Tags: concours
Template: job_offer
Job_Location: Orsay (91) - France
Job_Duration: 
Job_Website: http://www.idris.fr
Job_Employer: CNRS - IDRIS
Expiration_Date: 2020-07-02
Attachment: 

Dans le cadre des concours de recrutement externes CNRS 2020, l&#39;IDRIS recrute:

**un assistant en gestion administrative (H/F), **
**assistant ingénieur, concours 174**

La personne recrutée aura en charge la gestion administrative des comptes utilisateurs, des demandes d&#39;autorisation de sécurité et des actes administratifs et financiers de l&#39;IDRIS.

Pour plus d&#39;informations sur les postes offerts au recrutement à l&#39;IDRIS, consultez notre site web : <http://www.idris.fr/annonces/idris-recrute.html>
