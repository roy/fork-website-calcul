Title: CPU time improvment for incremental forming
Date: 2020-11-10 09:44
Slug: job_f181f98f578abbbae0c6f0b7c4ef37f1
Category: job
Authors: Katia MOCELLIN
Email: katia.mocellin@mines-paristech.fr
Job_Type: Post-doctorat
Tags: postdoc
Template: job_offer
Job_Location: Sophia Antipolis
Job_Duration: 18 mois
Job_Website: https://www.cemef.minesparis.psl.eu/
Job_Employer: ARMINES - CEMEF - Mines ParisTech
Expiration_Date: 2021-04-02
Attachment: 

This post doctoral study is part of a partenarial project gathering

- an industrial partner aiming at produce large parts for aerospace or energy applications with the use of e new flow forming device, 
- a software editor providing computational tools for metal forming simulation
- A SME involved in the real time analysis of production;
- the CEMEF working on improvement of numerical methods for the simulation of the process.

The main objective is to develop numerical methods for reducing CPU time in incremental forming of thick plates’ simulations.
Uncoupling strategies for local plastic deformation with regard to global elastic displacement will be studied for the lagrangian approach. ALE formulation could also be improved with respect to fiabilisation of the computed mechanical response.
