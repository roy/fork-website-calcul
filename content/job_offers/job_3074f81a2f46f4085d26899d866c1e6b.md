Title: Precision auto-tuning and numerical validation
Date: 2021-05-12 13:08
Slug: job_3074f81a2f46f4085d26899d866c1e6b
Category: job
Authors: Fabienne Jézéquel
Email: Fabienne.Jezequel@lip6.fr
Job_Type: Thèse
Tags: these
Template: job_offer
Job_Location: LIP6, Sorbonne Université, Paris
Job_Duration: 3 ans
Job_Website: https://www-pequan.lip6.fr/~jezequel/OFFERS/PhD_precision_auto_tuning.pdf
Job_Employer: Sorbonne Université
Expiration_Date: 2021-09-30
Attachment: 

This PhD will be carried out in the framework of the Interflop ANR
project which aims at proposing a unified platform for the numerical
validation of large codes.
The subject is detailed in 
<https://www-pequan.lip6.fr/~jezequel/OFFERS/PhD_precision_auto_tuning.pdf>

