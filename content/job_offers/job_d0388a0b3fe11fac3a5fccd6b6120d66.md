Title: Administrateur-trice systèmes et réseaux 
Date: 2021-06-03 09:37
Slug: job_d0388a0b3fe11fac3a5fccd6b6120d66
Category: job
Authors: Geneviève Morvan
Email: communication@idris.fr
Job_Type: Concours
Tags: concours
Template: job_offer
Job_Location: Orsay (91)
Job_Duration: 
Job_Website: http://www.idris.fr/annonces/idris-recrute.html
Job_Employer: CNRS-IDRIS
Expiration_Date: 2021-06-30
Attachment: 

Dans le cadre des concours de recrutement externes CNRS 2021, l&#39;IDRIS recrute :

**Administrateur-trice systèmes et réseaux ( IE - concours 93 )**

Au sein de l&#39;équipe réseau de l&#39;IDRIS, la personne recrutée aura pour mission de participer à l&#39;administration de l&#39;infrastructure vitale (réseau et services) afin de sécuriser et garantir une disponibilité des ressources nationales de calcul, d&#39;administrer l&#39;ensemble des équipements et des serveurs pour les personnels du centre, de collaborer à des projets externes et inter-équipes et de maintenir des contacts avec les différents prestataires afin de faire évoluer les services. Le poste couvre aussi bien des activités réseau que système à part égale et le travail nécessite des compétences dans les deux domaines. 

Pour consulter la fiche complète du concours : <https://profilsdemplois.cnrs.fr/ords/afipprd/PG_PROFIL_PUBLIC_REFERENS.pAfficheProfilPublic?destination=CE2021&hidIdConcours=26202>

Plus d&#39;information sur les concours et/ou pour postuler, consulter le site web du CNRS : <http://www.dgdr.cnrs.fr/drhita/concoursita/>

Date limite : mercredi 30 juin 2021 à 13h
