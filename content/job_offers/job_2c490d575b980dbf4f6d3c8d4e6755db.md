Title: Réalité virtuelle et intelligence artificielle pour l’aide à la décision et la prévention des risques naturels
Date: 2020-06-11 08:34
Slug: job_2c490d575b980dbf4f6d3c8d4e6755db
Category: job
Authors: Benjamin Van Wyk de Vries
Email: Ben.VANWYK@uca.fr
Job_Type: Thèse
Tags: these
Template: job_offer
Job_Location: Clermont-Ferrand
Job_Duration: 
Job_Website: 
Job_Employer: UCA
Expiration_Date: 2020-06-27
Attachment: 

La thèse développera des outils de Realité virtuelle pour permettre à plusieurs utilisateurs d’être immergés sur une topographie sujette aux aléas, de simuler des phénomènes naturels d’envergure dans le but de minimiser les vulnérabilités socio-économiques. Le/a doctorant/e portera ces codes de simulation en réalité virtuelle et optimisera la simulation pour une visualisation en temps réel. Ensuite il/elle devra étudier le comportement et les réactions humaines face à ce type d’événement, au moyen de l’intelligence artificielle distribuée (système multi-agents ou moteur de règles), en intégrant la perception visuelle et auditive. L’analyse de données (topographie, météorologie, activité anthropique, coutumes, éthique…) en entrée d’un
algorithme de machine learning permettra d’établir des règles pour l’aide à la décision et
la prévention des risques naturels.

https://spi.ed.uca.fr/medias/fichier/thesised-van-wyk-de-vries-fr_1590683423776-pdf
https://spi.ed.uca.fr/medias/fichier/thesised-van-wyk-de-vries-en_1590683349525-pdf