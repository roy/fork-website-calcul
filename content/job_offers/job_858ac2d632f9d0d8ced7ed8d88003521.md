Title: Evaluation des approches hybrides basées sur des simulations numériques et de l’intelligence artificielle pour optimiser la conception de l’aérodynamique d’automobile
Date: 2021-06-08 14:35
Slug: job_858ac2d632f9d0d8ced7ed8d88003521
Category: job
Authors: Iraj MORTAZAVI
Email: iraj.mortazavi@lecnam.net
Job_Type: Thèse
Tags: these
Template: job_offer
Job_Location: CNAM Paris, Stellantis Velizy
Job_Duration: 36 mois
Job_Website: 
Job_Employer: CNAM Paris
Expiration_Date: 2021-08-31
Attachment: job_858ac2d632f9d0d8ced7ed8d88003521_attachment.pdf

(plus de détails dans le document joint)
URGENT: date limite de candidature 11/06/2021

L’objectif des travaux de thèse est d’évaluer le potentiel des méthodes IA pour l’optimisation de la trainée aérodynamique automobile.
Cette optimisation portera sur des variations du design extérieur de la 508, incluant les boucliers avant/arrière, le galbe des flancs, et les galbes du pavillon et de la lunette. La CAO de ce véhicule est déformable selon un grand nombre de paramètres de l’ordre de 200.
Les contraintes associées aux prestations présentées ci-dessous, seront également prises en compte :

- La stabilité dynamique du véhicule, qui requiert des niveaux de portances SCz suffisants pour assurer un appui aérodynamique en assiette « haute vitesse » et en assiette « phase de freinage »,
- Le refroidissement des freins, qui nécessite un arrosage en air suffisant, sur disques et étriers, et 
- Le style de la voiture qui doit rester attractif.
