Title: Physics-Informed Machine Learning in the context of seismic imaging
Date: 2020-04-16 13:58
Slug: job_d015917519c58b09497c20334c3b55c6
Category: job
Authors: Prof. Elie Hachem
Email: elie.hachem@mines-paristech.fr
Job_Type: Thèse
Tags: these
Template: job_offer
Job_Location: Fontainebleau
Job_Duration: 3 years
Job_Website: 
Job_Employer: MINES ParisTech
Expiration_Date: 2020-05-29
Attachment: job_d015917519c58b09497c20334c3b55c6_attachment.pdf

**Overview:**
Within the MINDS project (Mines Initiative for Numerics and Data Science) developed at MINES
ParisTech (Paris School of Mines), the objective is to fill the gap between Machine Learning and Physics-based approaches.
Machine Learning is growing very rapidly. After a possible learning step, the objective is to let the data speak. These approaches tend to forget the more traditional physics-based approaches. The objective of the PhD thesis is to develop, in the context of seismic imaging, an intermediate approach to preserve the physics [1]. Currently, the main contributions of Machine Learning to seismic processing are related to pre-processing steps (de-noising, picking, ...) but not really yet to the imaging part (determining the Earth&#39;s properties from surface measurements, a highly non-linear problem). The explicit introduction of physics within Machine Learning should fill this gap.

**Details:**
In 2019, Raissi et al., demonstrated how it is possible to combine Machine Learning approaches with more traditional physics approaches (Physics-Informed Neural Networks, PINN) [3]. The applications are related to the resolution of partial differential equations (i.e. direct problems) as well as to the resolution of inverse problems (determining the main parameters controlling the physical phenomena, for example the wave propagation, from a set of observations). The later approach will be developed here. On the one hand, deep neural networks are able in theory to describe any functions. Learning is usually a complex task and in physics-related problems, observations are rare and expensive to acquire. On the other hand, Machine Learning does not usually consider physics-based equations, a very useful source of information. As proposed in [3], a modified loss function in the neural networks contains several terms to ensure that the data predict the observations and that the laws of physics are fulfilled. This second term can be seen as a regularisation term, essential in practice to avoid any over-fitting in the case of noisy data. The auto-differentiation (back-propagation of the errors) within the neural networks provides a way to estimate the optimal parameters.

This approach is very attractive and will be extended and modified to be applicable in the context of seismic imaging. Seismic acquisition consists of activating a seismic source and of recording acoustic / elastic waves. The objective is to determine seismic velocity wave fields and any other parameters controlling the wave propagation within the sub-surface. In comparison with the first PINN applications, seismic imaging offers some particular aspects to be properly considered:

* Seismic wave are mainly propagative waves, meaning that the wave field is not smooth. In order to check that the wave field obeys the wave equation, the number of controlling points is a priori much larger than for a diffusive problem with a more regular solution;
* The traditional loss function in seismic imaging contains a large number of local minima. How
does the PINN approach behave? How is it possible to take advantage of the frequency content
of the data? In the classical approaches, the model estimation first relies on the low frequencies
and then enlarges the frequency spectrum, in order to avoid local minima. How could the neural
network benefit from this approach (e.g. a proxi for the modelling part)?
* Finally, the number of unknowns (number of parameters to be estimated) is potentially very
large (thousands or much more, as the parameters depend on the spatial coordinates). In the
first articles, only a few values were determined. How to play with the neural network to address
this issue? The Generative Adversarial Networks (GAN) could be very useful to determine the
optimal parameterisation [2].

The objective of the PhD thesis is to develop a novel Physics-informed Machine Learning approach in the context of seismic imaging. The validations will be performed on synthetic and real data sets.

**Main references:**

1. Chauris, H. (2019). Full Waveform Inversion, in Seismic Imaging, a practical approach , J-L. Mari and M. Mendes (Eds.), EDP Sciences, chapter 5, 23 p., ISBN (ebook): 978-2-7598-2351-2, doi:10.1051/978-2-7598-2351-2.c007
2. Goodfellow, I., J. Pouget-Abadie, M. Mirza, B. Xu, D. Warde-Farley, S. Ozair, A. Courville and Y. Bengio (2014). Generative Adversarial Networks. Proceedings of the International Conference on Neural Information, arXiv:1406.2661
3. Raissi, M., P. Perdikaris, G.E. Karniadakis (2019). Physics-informed neural networks: A deep learning framework for solving forward and inverse problems involving nonlinear partial differential equations. Journal of Computational Physics, 378, 686-707

**Keywords:** Machine learning, Physics-informed Machine Learning (PINN), deep neural networks, seismic imaging, wave propagation

**Competences:**

The candidate should have a strong background in maths and physics. He/she should have a clear interest for Machine Learning and for geophysical applications, in particular in the context of seismic imaging. He/she should have a strong experience in scientific programming. It is appreciated if he/she also has some knowledge on high performance computing (HPC). It is essential to be fluent in English speaking and writing.

**Contacts**:

* PhD supervior: Pr. Hervé Chauris, Centre de Géosciences, MINES ParisTech
(<herve.chauris@mines-paristech.fr>, +33 1 64 69 49 13)
* PhD supervior: Pr. Elie Hachem, Groupe CFL, CEMEF, MINES ParisTech (<elie.hachem@minesparistech.
fr>, +33 4 93 95 74 58)
* Co-supervisor: Nicolas Desassis, Centre de Géosciences, MINES ParisTech
(<nicolas.desassis@mines-paristech.fr>, +33 1 64 69 47 73)

**Main location:** Centre de Géosciences, MINES ParisTech, 35, rue Saint-Honoré, Fontainebleau, France

**How to apply:**
Please send by email to <herve.chauris@mines-paristech.fr> and before the 29th of May 2020 (in pdf format):

* A resume
* A motivation letter
* At least a recommendation letter (or the name of a person to contact)
* A copy of a research report (e.g. master report)
