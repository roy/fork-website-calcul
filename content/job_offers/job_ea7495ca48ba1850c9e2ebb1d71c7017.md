Title: Génération automatique de code pour l’algèbre linéaire rapide sur GPU
Date: 2020-09-22 17:03
Slug: job_ea7495ca48ba1850c9e2ebb1d71c7017
Category: job
Authors: Denis Gueyffier (ONERA) & Albert Cohen (Google)
Email: denis.gueyffier@onera.fr
Job_Type: Post-doctorat
Tags: postdoc
Template: job_offer
Job_Location: Chatillon et Paris
Job_Duration: 2 ans
Job_Website: http://www.onera.fr
Job_Employer: ONERA et Google AI Paris
Expiration_Date: 2020-12-31
Attachment: 

Nous offrons un post-doctorat de 2 ans en temps partagé entre l&#39;ONERA et Google AI Paris. Merci de contacter par e-mail <denis.gueyffier@onera.fr> et <albertcohen@google.com>

Intitulé : Génération automatique de code pour l’algèbre linéaire rapide sur GPU

L&#39;ONERA travaille à la génération automatique de code pour un futur logiciel de simulation en mécanique des fluides [1]. Le but de ces travaux est en particulier de fournir des paradigmes de programmation de haut niveau évitant au développeur de réaliser lui-même les optimisations nécessaires pour obtenir des performances HPC élevées. L&#39;ONERA cherche aussi à assurer automatiquement la portabilité vers différents langages et différentes architectures matérielles actuelles (CPU multi-coeurs, GPU) et futures.

Par ailleurs, Google AI travaille à une approche générique pour la génération de code qui consiste à utiliser une approche hiérarchique à plusieurs niveaux de représentation (&#34;intermediate representation&#34;) [2]. L&#39;une des ambitions de cette approche est d&#39;obtenir un code optimal pour l&#39;exécution sur différents types d’architectures matérielles.

Ce travail en collaboration entre l&#39;ONERA et Google AI, visera à la résolution rapide de problèmes d&#39;algèbre linéaire issus des équations de la mécanique des fluides en écoulements compressibles discrétisées sur des maillages structurés. Nous chercherons à générer automatiquement un code optimisé pour l&#39;exécution sur GPU. Pour cela nous mettrons en place et étendrons potentiellement le formalisme du dialecte LinALG du framework MLIR [3, 4] sur des problèmes d&#39;algèbre linéaire de difficulté croissante. Pour guider la génération automatique ultérieure, nous devrons réaliser une analyse des goulots d&#39;étranglement à chaque niveau de parallélisme (warps, blocs, threads, vectorisation). Nous étudierons les décisions d&#39;optimisation qui peuvent être prises pour générer du code, les choix des différents niveaux de parallélisme, les choix d’ordonnancement des boucles, les choix de tuilage (cache-blocking) ou de vectorisation de boucles. Après l&#39;étude de cas simples, nous étendrons l&#39;approche à la résolution par méthode de Gauss-Seidel (itération de l&#39;application à un vecteur d&#39;une décomposition LU de la matrice initiale), dans une approche de type wavefront [5] qui sera adaptée aux spécificités des GPU.

Profil et compétences recherchées

Docteur en sciences dans le domaine de la simulation numérique ou des langages de programmation/compilation,

Expérience de la programmation parallèle en mémoire partagée sur CPU multi-coeurs ou GPU,

Compétences en algèbre linéaire numérique fortement appréciées.

Références

[1] B. Maugars, S. Bourasseau, C. Content , B. Michel, B. Berthoul, P. Raud, L. Hascoët, Algorithmic Differentiation for an efficient CFD solver, soumis à Journal of Computational Physics

[2] <https://pliss2019.github.io/albert_cohen_slides.pdf>

[3] <https://www.tensorflow.org/mlir>

[4] Ulysse Beaugnon, Basile Clément, Nicolas Tollenaere, Albert Cohen, On the Representation of Partially Specified Implementations and its Application to the Optimization of Linear Algebra Kernels on GPU, ArXiv <https://arxiv.org/pdf/1904.03383>

[5] R. Barrett, M.W. Berry, T.F. Chan, J. Demmel, J. Donato, J. Dongarra, V. Eijkhout, R. Pozo, C. Romine, H. Van der Vorst, Templates for the Solution of Linear Systems: Building Blocks for Iterative Methods, SIAM, Jan 1, 1994


