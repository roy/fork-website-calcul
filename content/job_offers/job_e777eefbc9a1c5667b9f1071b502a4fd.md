Title: Simulation numérique de la propagation du bang sonique
Date: 2019-12-06 11:57
Slug: job_e777eefbc9a1c5667b9f1071b502a4fd
Category: job
Authors: Florent RENAC
Email: florent.renac@onera.fr
Job_Type: Stage
Tags: stage
Template: job_offer
Job_Location: Châtillon, France
Job_Duration: 5 mois
Job_Website: https://w3.onera.fr/stages/sites/w3.onera.fr.stages/files/daaa-2020-25.pdf
Job_Employer: ONERA
Expiration_Date: 2020-02-28
Attachment: job_e777eefbc9a1c5667b9f1071b502a4fd_attachment.pdf

**Mots clés :** méthode de Galerkin discontinue, décomposition d&#39;opérateurs, équations d&#39;Euler et de Navier-Stokes compressibles, onde de choc, acoustique non linéaire

**Contexte et objectifs :** Le sujet de ce stage s&#39;inscrit dans le cadre d&#39;un projet de recherche sur la simulation de la propagation du bang sonique dans l&#39;atmosphère. Son objectif est de mieux comprendre les mécanismes par lesquels l&#39;onde de choc générée par un avion en vol supersonique se propage jusqu&#39;au niveau du sol. Ceci doit permettre la conception d’avions supersoniques civils dits «low boom» permettant d&#39;atténuer le bruit du bang et limitant ainsi la gêne acoustique lors du vol au dessus des zones habitées.

La simulation de ce phénomène requiert l&#39;utilisation de méthodes numériques très précises car l&#39;onde de choc est de faible intensité et est propagée sur de très grandes distances (plusieurs kilomètres). Dans ce contexte, on propose d&#39;utiliser une méthode de Galerkin discontinue qui constitue une discrétisation spatiale d&#39;ordre élevé où l&#39;on recherche une solution approchée sous la forme de polynômes par morceaux dans chaque élément du maillage. Celle méthode possède en effet des propriétés de faibles dissipation et dispersion essentielles aux simulations de propagation acoustique.

Cette méthode souffre néanmoins de problèmes de stabilité lorsqu&#39;elle est appliquée aux équations d&#39;Euler compressibles du fait principalement d&#39;erreurs de troncature des termes non linéaires. Ces erreurs conduisent à un repliement spectral qui affecte la qualité des résultats et peut également conduire à des solutions non physiques.

Une méthode de stabilisation existe basée sur une décomposition d&#39;opérateurs des flux de convection [1] qui utilise une propriété dite SBP (Summation By Parts) qui conserve au niveau discret l&#39;intégration par partie utilisée dans la formulation variationnelle. On se propose ici d&#39;étendre cette méthode à une formulation en petites perturbations des équations non-linéaires d&#39;Euler et de Navier-Stokes compressibles utilisées classiquement pour les simulation de propagation acoustique.

**Description du travail :** On analysera les propriétés théoriques de stabilité, dispersion et dissipation du schéma numérique et leurs effets sur la propagation des ondes acoustiques. Des limiteurs de pente sur maillage non structuré seront également envisagés pour stabiliser la méthode en présence de discontinuités de la solution. Ces techniques seront mises en œuvre dans le code de calcul AGHORA de l&#39;ONERA et éprouvées par le biais d&#39;expériences numériques. Les effets des non-linéarités et de la viscosité physique sur la structure des ondes propagées seront également analysés.

[1] T. Chen and C.-W. Shu, Entropy stable high order discontinuous Galerkin methods with suitable quadrature rules for hyperbolic conservation laws, J. Comput. Phys., 345 (2017), pp. 427–461.