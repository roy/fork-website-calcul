Title: Ingénieur système  - stockage, cloud (PaaS/Kubernetes) et HPC
Date: 2019-12-12 14:44
Slug: job_f67ba9ec463238d3cec0fd6fecd2d59a
Category: job
Authors: Sylvain Ferrand et David Delavennat
Email: informatique@cmap.polytechnique.fr
Job_Type: CDI
Tags: cdi
Template: job_offer
Job_Location: Palaiseau
Job_Duration: 
Job_Website: https://www.polytechnique.edu/fr/ingenieur-systeme-et-reseau-stockage-fh
Job_Employer: Ecole Polytechnique
Expiration_Date: 2020-03-05
Attachment: 

Au sein de l’unité de service IDCS (Infrastructure, Données et Calcul scientifique)  de l&#39;Ecole Polytechnique&nbsp;:

Activités principales :

* Participer au déploiement d’une infrastructure PaaS et des applications pour l’enseignement et la recherche (Jupyter) ;
* Participer à la gestion opérationnelle d’un cluster de calcul HPC
* Réaliser une étude d’impact, le dimensionnement et le chiffrage d&#39;une solution de stockage capacitif à l&#39;échelle de l&#39;Ecole polytechnique

Activités complémentaires :

* Collaborer avec la DSI sur le déploiement de nouveaux outils et sur la participation aux nouveaux projet ;
* Participer à la mise en place de formations autour des outils de calcul.



Tous nos postes sont accessibles aux personnes en situation de handicap.
