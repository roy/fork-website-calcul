Title: Reconstruction tomographique multi-contraste et multi-dimensionnelle par intelligence artificielle
Date: 2021-03-09 11:42
Slug: job_4906f72f0df3497a5fdf2d0fd13e5ae9
Category: job
Authors: Kévin GINSBURGER
Email: kevin.ginsburger2@cea.fr
Job_Type: Thèse
Tags: these
Template: job_offer
Job_Location: CEA DAM ILE-DE-FRANCE Bruyères-le-Châtel 91297 ARPAJON CEDEX
Job_Duration: 3 ans
Job_Website: http://www-instn.cea.fr/formations/formation-par-la-recherche/doctorat/liste-des-sujets-de-these/reconstruction-tomographique-multi-contraste-et-multi-dimensionnelle-par-intelligence-artificielle,2.html
Job_Employer: CEA - DAM Ile de France
Expiration_Date: 2021-09-01
Attachment: job_4906f72f0df3497a5fdf2d0fd13e5ae9_attachment.pdf

La radiographie X éclair est utilisée au CEA-DAM dans le cadre d’expériences d’hydrodynamique pour observer des objets à forte variation de densité sur des temps d’observation très courts. Ces radiographies ont pour but de reconstruire l’objet radiographié en densité ainsi que les interfaces associées.

Actuellement, l’installation franco-britannique EPURE (située sur le centre du CEA Valduc) utilisée pour nos applications ne possède qu’un seul axe radiographique. Ainsi, des hypothèses géométriques simplificatrices sur l’objet doivent être réalisées pour la reconstruction (axi-symétrie par exemple) mais dans ce cas la présence de singularités 3D est mal considérée. Prochainement, l’introduction de 2 axes supplémentaires sur l’installation EPURE amènera des projections supplémentaires pour la reconstruction.

Par ailleurs, la méthode d’imagerie actuelle, basée sur le contraste en absorption, fournit des images dont les contrastes peuvent être très faibles. Une technique déjà éprouvée pour des applications médicales et industrielles, basée sur l’exploitation de la phase des rayons X permet d’obtenir des informations complémentaires au niveau des interfaces entre les matériaux.

La prise en compte d’un nombre de vues supplémentaires et/ou de différents types de contrastes (absorption et phase) conduit à vouloir fusionner toutes ces informations pour une reconstruction plus fine de l’objet. Dans ce contexte, les techniques d’intelligence artificielle (IA) fournissent un cadre naturel pour développer cette nouvelle approche de reconstruction tomographique.

La première partie de la thèse visera à prendre en main les outils de reconstruction et de détection de contours basés sur l’intelligence artificielle actuellement développés au laboratoire. Ces outils devront être adaptés afin d’intégrer l’information issue du contraste de phase lors de la reconstruction. Cela impliquera notamment la constitution d’une base de données issues de simulations numériques incluant à la fois les contrastes en absorption et en phase.

Dans un second temps, la reconstruction tomographique pourra être étendue à l’utilisation de plusieurs projections spatiales. Après le développement de l’outil de reconstruction et sa validation par simulation, le doctorant participera à la validation expérimentale du code en testant sa capacité à traiter des images issues d’expériences réalisées sur une des installations radiographiques de la DAM (ELSA). Il s’agira d’estimer la qualité de détection et de reconstruction des détails fins de l’objet imagé et des interfaces en utilisant une maquette préalablement fabriquée au laboratoire.