Title: Task-based parallelisation of a 3D anisotropic mesher
Date: 2021-04-20 08:53
Slug: job_c06ad44ecff253d09faf1b9381dc5119
Category: job
Authors: Luisa Silva
Email: Luisa.Rocha-Da-Silva@ec-nantes.fr
Job_Type: Thèse
Tags: these
Template: job_offer
Job_Location: École Centrale de Nantes
Job_Duration: 3 years
Job_Website: https://ici.ec-nantes.fr/english-version/join-us
Job_Employer: Institut de Calcul Intensif – École Centrale de Nantes
Expiration_Date: 2021-07-13
Attachment: job_c06ad44ecff253d09faf1b9381dc5119_attachment.pdf

IciMesh is a mesh generator, under Open Source license, based on topology optimization andquality criteria that generates good quality simplex type elements (2d triangles and 3d tetrahedra),in an unstructured and non-hierarchical way. A parallel version of this mesh generator alreadyexists and uses the MPI parallelization library.

In IciMesh, the remeshing operators are very local: there is a topology optimization around a nodeor an edge. The order in which these operators are executed is not important, so the algorithm isnicely parallelized.The current parallelization via MPI consists in cutting the mesh into subdomains and applying toeach of them the sequential mesh generator under the constraint of not touching the interfacesbetween them. A parallel repartitioning phase moves these interfaces and, by iteration of theprevious steps, we get an adapted mesh over the entire domain. From a global point of view, thiscan be seen as a reordering of the main operators so as to be able to exploit the locality of the dataand, consequently, the parallelism.In the case of parallelization by spots, we will follow an identical strategy but in a context of sharedmemory. In the currently sequential engine of the mesh generator, it will be a question of over-partitioning the domain so as to define different zones which can be treated by the atomic operatorsindependently. Two strategies are possible:- over-partition and define fairly compact areas which will be assigned to a thread and re-meshunder the constraint of not touching the interfaces between the areas, then finish by processing theinterfaces (identical to the pure MPI approach but in a context shared memory). This strategyseems, however, limited to a fairly low number of threads (CPUs);- color each node and edge of the mesh so as to be able to execute on each node or edge of the samecolor the main operators without exchange between them. If each color contains a large enoughnumber of nodes or edges (&gt; 1000, 10,000), operators can be performed on different XeonPhi orGPU computation units. The final algorithm will be to carry out a loop on the different colors and,at each iteration, to launch in parallel the remeshing kernel adapted to the desired hardware.