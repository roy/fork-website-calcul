Title: Ingénieur en calcul numérique sur CPU/GPU
Date: 2020-06-22 09:17
Slug: job_537ccbb6a94fad53f2c55db90ad6e9de
Category: job
Authors: Julien Alexandre dit Sandretto
Email: alexandre@ensta.fr
Job_Type: CDD
Tags: cdd
Template: job_offer
Job_Location: Palaiseau
Job_Duration: 12 mois
Job_Website: 
Job_Employer: ENSTA Paris
Expiration_Date: 2020-08-30
Attachment: 

Dans le cadre d’un projet de pré-maturation (avant la création d’un produit à partir de résultats de recherche), l&#39;ENSTA Paris recrute un ingénieur en calcul numérique sur CPU/GPU.

Plus de détails ici : <https://perso.ensta-paristech.fr/~chapoutot/ssh-website/docs/ENSTA_ingenieur_gpu.pdf>

