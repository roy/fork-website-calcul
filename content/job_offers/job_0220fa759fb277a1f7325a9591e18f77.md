Title: Traitement Automatique du Langage Naturel Application au domaine de la santé
Date: 2019-12-04 10:45
Slug: job_0220fa759fb277a1f7325a9591e18f77
Category: job
Authors: ALLIOD Charlotte
Email: charlotte.alliod@altran.com
Job_Type: Stage
Tags: stage
Template: job_offer
Job_Location: LYON, Vaise
Job_Duration: 6 mois
Job_Website: 
Job_Employer: Altran
Expiration_Date: 2020-02-26
Attachment: job_0220fa759fb277a1f7325a9591e18f77_attachment.pdf

**Contexte :**

Face à l’explosion du monde du numérique, et notamment l’intégration de l’Intelligence Artificielle et du Big Data, le processus de soins et de prise en charge du patient doit être repensé. L’émergence de ces technologies rend en effet possible l’amélioration de la conception des nouveaux projets de santé, l’optimisation du parcours de soin au sein de structures hospitalières ainsi que le développement de nouveaux dispositifs médicaux innovants, connectés et adaptés permettant ainsi un meilleur suivi des pathologies chroniques. 

Dans ce cadre, la Direction R&amp;I France, entité de recherche interne du groupe Altran, mène le projet PACIFIC (PAtient Centered Innovations For the Improvement of Care). Ce projet vise à développer un vaste ensemble d’algorithmes d’analyse de données et de Machine Learning pour le domaine de la santé, allant du Process Mining, l’analyse de Séries Temporelles, au Text Mining.

**Mission :**

La mission consiste à mettre en place un ensemble d’outils de Natural Language Understanding (NLU) permettant la reconnaissance automatique d’informations contenues dans du texte. Ce projet sera basé sur un corpus de textes issus d’un environnement médical, et servira à renforcer les modèles de Natural Language Processing (NLP) déjà développés sur ce cas d’usage au sein de la Direction R&amp;I d’Altran dans le cadre du projet PACIFIC. 

Le stage s&#39;organisera de la manière suivante :

- Modélisation du problème (notions de graphes de connaissance, ontologies, reconnaissance d’entités nommées, extractions de relations)
- Elaboration d’un état de l’art basé sur la littérature scientifique et les solutions open-source existantes
- Test de solutions existantes sur le corpus proposé
- Recherche et implémentation de nouveaux algorithmes
- Comparaison des performances sur le corpus proposé
- Communication interne d’une synthèse de cette étude 

**Profil :**

- Formation ingénieur / master avec spécialisation en Data Science, Statistiques, Informatique
- Intérêt pour la R&amp;D, autonomie technique et esprit d&#39;initiative
- Connaissance en Machine Learning, Deep Learning et en développement en langage Python (Scikitlearn, Pytorch)
- Des connaissances en Text Mining serait un plus
- Maîtrise du français et/ou de l’anglais
