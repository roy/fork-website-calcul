Title: Sensitivity analysis for thermohydrodynamic models: uncertainty analysis and parameter estimation
Date: 2021-01-26 08:13
Slug: job_6f6f026232433bb7439cff35d9604691
Category: job
Authors: Puscas Maria Adela
Email: maria-adela.puscas@cea.fr
Job_Type: Thèse
Tags: these
Template: job_offer
Job_Location: Saclay
Job_Duration: 3 ans
Job_Website: 
Job_Employer: CEA
Expiration_Date: 2021-05-30
Attachment: job_6f6f026232433bb7439cff35d9604691_attachment.pdf

The sensitivity analysis (SA) studies are essential for many engineering applications, such as uncertainty quantification, optimal design, and to answer what if questions, i.e., what happens to the model’s solution if the input parameters change. These tasks can be performed in many different ways, depending on the nature of the model considered. In this work, we consider systems that can be modeled with partial differential equations (PDEs). The sensitivity variable itself is defined as the derivative of the state (i.e., the output of the model) with respect to the parameters of interest.

In the thermohydrodynamic models, the SA can be used to determine how the model response in a point is affected by a change in initial conditions or limits, or to any other physical parameter such as the viscosity, the heat capacity, thermal diffusivity, etc. In previous work [1], an uncertainty quantification analysis for the Navier-Stokes equations using a sensitivity analysis technique model was realized. The SA provides first-order estimates of average and variance of the velocity field when some parameters are uncertain; then, it uses the variance to compute confidence intervals for the model’s output. This goes under the name of forward uncertainty propagation, which is part of the broader field of uncertainty quantification(UQ). Methods of uncertainty propagation based on SA are particularly efficient in terms of computational time. However, since SA is based on Taylor expansions of the state variable with respect to the parameter of interest, these methods are intrinsically local: they can be used only for random variables with a small variance.

Previous work [1] shows that sensitivity analysis can be used instead of a large number of Monte Carlo simulations in the case of laminar flows. This doctoral project’s main objectives are to extend this previous work to the turbulent flows. Here, we consider the RANS (Reynolds-Averaged Navier-Stokes) equations. The speed is decomposing into an average component and a fluctuating component. Many modelizations of the Reynolds stress tensor are possible. Still, in the current project, we are interested in the more classical one, based on the Boussinesq hypothesis, the two-equations (k, eps) model [2]. This work is particularly challenging due to the nonlinearity of the turbulence model. This implies, in particular, the stability estimates analysis for the sensitivity RANS system. Besides, an analysis of the convergence when the finite elements volumes method (FEV) is applied to discretize the sensitivity RANS system is necessary. Finally, the subject, therefore, proposes to apply the sensitivity analysis to representative thermohydrodynamic industrial applications. The thesis will be carried out at the Thermohydraulics and Fluid Mechanics Department of CEA/Saclay. 

[1] C. Fiorini, B. Després, and M. A. Puscas. Sensitivity equation method for the Navier-Stokes equations applied to uncertainty propagation. International Journal for Numerical Methods in Fluids, 2020.
[2] B. Launder and D. Spalding, The numerical computation of turbulent flows. Computer Methods in Applied Mechanics and Energy, 1974
