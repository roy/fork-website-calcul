Title: High Performance Deep Reinforcement Learning
Date: 2020-12-07 09:43
Slug: job_712e92fa640951a2e5c1ccc2c60cf465
Category: job
Authors: Bruno Raffin
Email: bruno.raffin@inria.fr
Job_Type: Post-doctorat
Tags: postdoc
Template: job_offer
Job_Location: Grenoble et/ou Lille
Job_Duration: 2 ans
Job_Website: https://jobs.inria.fr/public/classic/fr/offres/2020-03122
Job_Employer: INRIA
Expiration_Date: 2021-03-01
Attachment: 

We are looking for a postdoc for a 2 year contract  between  the  INRIA teams Datamove (Grenoble - <https://team.inria.fr/datamove/>) and Sequel (Lille - <https://team.inria.fr/sequel/>) to work on the convergence between AI and HPC and more specifically  on High Performance Deep Reinforcement Learning. 

For details and to apply: <https://jobs.inria.fr/public/classic/fr/offres/2020-03122>
