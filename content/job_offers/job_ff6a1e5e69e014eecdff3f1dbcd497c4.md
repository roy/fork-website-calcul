Title: Développeur·euse C++/QML
Date: 2021-07-02 14:16
Slug: job_ff6a1e5e69e014eecdff3f1dbcd497c4
Category: job
Authors: Sonia Portier
Email: emploi@nextflow-software.com
Job_Type: CDI
Tags: cdi
Template: job_offer
Job_Location: Nantes, France
Job_Duration: 
Job_Website: https://www.nextflow-software.com/
Job_Employer: Nextflow Software
Expiration_Date: 2021-09-24
Attachment: job_ff6a1e5e69e014eecdff3f1dbcd497c4_attachment.pdf

Editeur de logiciels basé à Nantes, filiale de Siemens Digital Industry Software, Nextflow Software développe et commercialise des logiciels d’ingénierie assistée par ordinateur (CAE) dans le domaine de la dynamique des fluides (CFD).

Nextflow s’adresse aux sociétés d’ingénierie et industriels développant et fabriquant des produits et des systèmes impliquant des écoulements de fluides, potentiellement avec des géométries complexes et des interactions avec des solides, dans le secteur de l’automobile, l’aéronautique, le maritime, etc. Grâce à son équipe hautement qualifiée composée d’ingénieurs et de docteurs et à un partenariat académique de plus de 10 ans avec les laboratoires de l’Ecole Centrale de Nantes (ECN), Nextflow Software ouvre de nouvelles perspectives dans le domaine de la simulation hydrodynamique.

**Contexte** :
Nextflow Software souhaite agrandir son équipe de développement pour le produit Nextflow Studio !
Nextflow Studio est un logiciel fournissant un environnement graphique intuitif permettant la mise en données et la validation de calculs de simulations.
Il supporte les formats CAD les plus courants et propose des outils performants de maillage.

**Poste et Missions**
Au sein de l’équipe de développement de Nextflow Studio, vous participerez à toutes les phases de développement du produit en méthode Agile.
Dans ce contexte, vous devrez :

- Participer à l’analyse des besoins avec les différents acteurs du produit (équipe QA, product manager, équipe support, etc.)
- Suivre et proposer les bonnes pratiques en termes d’ergonomie GUI
- Contribuer à l’élaboration des solutions techniques
- Développer les composants et les tests unitaires, participer à la maintenance corrective
- Participer à la rédaction les documentations techniques et utilisateurs et à la validation
- Faire la promotion interne du produit et le support auprès des autres équipes

**Votre Profil**
De formation ingénieur, master ou docteur en informatique, vous bénéficiez d&#39;une première expérience en tant que développeur C++ pour des logiciels industriels.
Vous avez le sens du service, aimez le travail en équipe ?
Vous aimez travailler dans un contexte scientifique poussé ? 
Vous aimez travailler en méthode agile ?
Apprendre et partager vos connaissances ?
N’hésitez pas à postuler !

Les connaissances indispensables pour ce poste sont :

- C++ 17
- Qt/QML
- OOP/Design Pattern/modern C++ idioms
- Anglais technique

Les connaissances appréciées sont :

- Git
- CMake
- Python
- JIRA
