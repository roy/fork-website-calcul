Title: Ing. Segment Sol mission et traitement de données massives
Date: 2020-10-16 11:08
Slug: job_e8a686588149872c9b45ffee47ea1bd8
Category: job
Authors: Guillaume Eynard-Bontemps
Email: guillaume.eynard-bontemps@cnes.fr
Job_Type: CDI
Tags: cdi
Template: job_offer
Job_Location: Toulouse
Job_Duration: 
Job_Website: https://recrutement.cnes.fr/fr/annonce/1044193-ing-segment-sol-mission-et-traitement-de-donnees-hf-31400-toulouse
Job_Employer: CNES
Expiration_Date: 2021-01-08
Attachment: 

Les volumes de données produits par les différents projets satellites deviennent des enjeux stratégiques tant sur la gestion de ces volumes que les modes de traitement de ces données. Dans ce contexte, le CNES recherche son futur(e) ingénieur(e) Etudes et Développement Segment sol mission et traitement de données/IA.

Votre objectif principal sera d’assurer le support à l&#39;architecture logicielle et l’intégration des traitements pour les projets : GeoDataHub et PEPS (Observation de la Terre) afin d’aider les métier thématiques (Observation de la Terre, Science, Telecom, Lanceur…)  du CNES mais également les laboratoires partenaires.

Intégré(e) à l’équipe projet, vous collectez les besoins en terme de traitement et de gestion de la donnée, proposez et prototypez de nouvelles fonctionnalités innovantes en terme de gestion/traitement de la donnée, Intelligence Artificielle ou d&#39;architecture logicielle.

En lien étroit avec les responsables des chaînes de traitement, vous contribuez à l’intégration de ces chaînes sur des moyens mutualisés et/ou dédiés au projet. Garant de l’adéquation des solutions proposées vis-à-vis du besoin des métiers, vous pouvez également rédiger les spécifications de développement et en assurer le suivi industriel (revues, points techniques, recettes).

De par votre expertise, vous participez aux projets relatifs à la gestion de la donnée (DataLake, STAF-Stockage long terme, SEF-Système d’échange de Fichiers…)  et assurez la cohérence avec les projets des autres structures métier.

Vous menez les travaux de convergence/rationalisation des solutions techniques, effectuez la veille technologique ainsi que les R&amp;T sur le domaine et assurez le dialogue avec les entités portant les même problématiques (MétéoFrance, Airbus, CLS…)

Pour garantir le bon fonctionnement avec nos partenaires, vous êtes le référent sur la politique de standardisation du CNES auprès de groupes internationaux.

**Profil de candidat recherché :**

Bac+5 (Ingénieur(e) ou universitaire), vous avez un bon esprit d’analyse et de synthèse vous permettant d’appréhender le fonctionnement d’un système complexe.

Vous avez une première expérience réussie d&#39;au moins 4 ans qui vous a permis de développer vos compétences en méthodes de développement (Linux, Python, Java, C), en calcul scientifique et en calcul haute performance..

Vous avez une vision orientée service/support et une forte appétence pour innover, tester…

Une connaissance des technologies d’intelligence artificielle (supervisés, semi-supervisés, non-supervisés, renforcement…) et des outils associés (TensorFlow…) serait appréciable.