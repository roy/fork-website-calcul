Title: Stage Ingénieur ou de M2: méthode d&#39;éléments finis multi-échelles pour les écoulements
Date: 2020-12-03 14:46
Slug: job_d3d7242f1b92fea81068fe29e36264f0
Category: job
Authors: Pascal Omnes
Email: pascal.omnes@cea.fr
Job_Type: Stage
Tags: stage
Template: job_offer
Job_Location: CEA Saclay
Job_Duration: 6 mois
Job_Website: 
Job_Employer: Commissariat à l&#39;énergie atomique et aux énergies alternatives
Expiration_Date: 2021-01-08
Attachment: 

Stage Ingénieur ou de M2: méthode d&#39;éléments finis multi-échelles pour les écoulements

6 mois: Mars -- Août ou Avril -- Septembre 2021

Personne à contacter: Pascal Omnes - CEA Saclay pascal.omnes@cea.fr


Un milieu multi-échelles est un milieu dans lequel les phénomènes physiques sont influencés par (au moins) deux échelles spatiales et/ou temporelles:
- écoulements dans les milieux poreux (ex: sous-sol) dont la composition varie rapidement (par exemple sur l&#39;échelle de quelques mètres) par rapport à l&#39;échelle d&#39;observation (de l&#39;ordre du kilomètre),
- écoulements dans les milieux encombrés de nombreux obstacles (ex: coeur de réacteur nucléaire avec les crayons combustibles par milliers).

Les méthodes d&#39;éléments finis usuelles peinent à simuler de façon efficace les phénomènes physiques ayant lieu dans ces milieux. Si l&#39;on note &#34;e&#34; l&#39;échelle spatiale (adimensionnée) la plus fine qui influence les phénomènes, et si e &lt;&lt; 1, alors il est nécessaire de discrétiser le domaine de calcul à l&#39;aide de maillages dont la taille de maille caractéristique vérifie h &lt;&lt; e &lt;&lt; 1; il est donc évident que de telles simulations vont nécessiter des ressources informatiques coûteuses, particulièrement en dimension trois d&#39;espace.

 Les éléments finis multi-échelles proposent de travailler sur un maillage grossier de pas H &gt;&gt; e, mais avec des fonctions de base &#34;bien adaptées&#34; (à la place des fonctions de base polynomiales usuelles), i.e. résolvant un problème proche de l&#39;équation aux dérivées partielles de départ sur des sous-maillages fins (h &lt;&lt; e) des cellules du maillage grossier. En pratique, ces méthodes nécessitent:
- une méthodologie précise pour définir judicieusement les fonctions de base,
- la mise en oeuvre du calcul de ces fonctions de base par résolution sur un maillage fin d&#39;une équation proche de l&#39;équation de départ,
- l&#39;assemblage et la résolution du problème sur le maillage grossier.

Notons que les deux derniers points ci-dessus peuvent faire appel au calcul parallèle, dans la mesure où les fonctions de base sont indépendantes les unes des autres et peuvent donc être calculées sur autant de processeurs qu&#39;il y en a de disponibles.

Le travail attendu comprend, sans y être limité, les aspects suivants:
- sur des problèmes modèles (diffusion et convection diffusion) et en 1D:
- définition des fonctions de base,
- calcul effectif, par approximation numérique sur maillage fin, de ces fonctions de base,
- assemblage et résolution du problème grossier,
- analyse d&#39;erreur du schéma ainsi obtenu.
- Passage en 2D.
 
Un aspect important à observer est ce qu&#39;il se passe lorsque la convection domine la diffusion: les éléments finis usuels sont sujets à des phénomènes d&#39;oscillations numériques parasites si le maillage n&#39;est pas suffisamment fin. Il faudra quantifier l&#39;apport des éléments finis multi-échelles sur cette question.

**Le stage pourra, sous condition, se poursuivre par une thèse débutant en Octobre 2021, dont le sujet est décrit sur la page**
<http://www-instn.cea.fr/formations/formation-par-la-recherche/doctorat/liste-des-sujets-de-these/methode-d-elements-finis-multi-echelles-pour-les-ecoulements-incompressibles,21-0077.html>