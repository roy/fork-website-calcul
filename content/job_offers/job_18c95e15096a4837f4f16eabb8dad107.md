Title: Multi-level multi-domain PIC simulation of a laser wakefield accelerator
Date: 2020-03-12 11:11
Slug: job_18c95e15096a4837f4f16eabb8dad107
Category: job
Authors: Arnaud Beck
Email: beck@llr.in2p3.fr
Job_Type: Post-doctorat
Tags: postdoc
Template: job_offer
Job_Location: Laboratoire Leprince-Ringuet
Job_Duration: 2 ans
Job_Website: https://emploi.cnrs.fr/Offres/CDD/UMR7638-ARNBEC-001/Default.aspx
Job_Employer: CNRS
Expiration_Date: 2020-06-05
Attachment: job_18c95e15096a4837f4f16eabb8dad107_attachment.pdf

Post doctoral position for the implementation of advanced numerical methods in the open source Particle-in-cell code Smilei.  The post-doctoral fellow will lead the development and integration of these methods which aim at offering refining capabilities (AMR like) to the code. The position starts in October 2020 and is available to young researchers ( &lt; 2 years after PhD). Details in the attached file. 