Title:  Ingénieur de Recherche Calcul Scientifique - Responsable du pôle Cesar 
Date: 2020-01-06 17:53
Slug: job_9b6802674d68474c229c02450e397c29
Category: job
Authors: Romaric DAVID
Email: david@unistra.fr
Job_Type: CDD
Tags: cdd
Template: job_offer
Job_Location: Strasbourg
Job_Duration: 1 an 
Job_Website: http://www.unistra.fr
Job_Employer: Université de Strasbourg
Expiration_Date: 2020-03-30
Attachment: job_9b6802674d68474c229c02450e397c29_attachment.pdf

Pourquoi ne pas prendre une excellente résolution pour 2020 en contribuant au rayonnement de l’Université de Strasbourg ? L’Université recrute un ingénieur de Recherche, responsable au sein de la Direction du Numérique du pôle  « Calcul et Services Avancés à la Recherche ».

Ce pôle CESAR (Calcul et Services Avancés à la Recherche) de la Direction du Numérique a pour objectif de soutenir les projets de données de la recherche, de la diffusion de la culture scientifique et du calcul intensif à l’Université.

Le responsable du pôle CESAR, avec son équipe, met en œuvre, développe et maintient le système d&#39;information soutenant le calcul scientifique et les projets de données et en garantit la sécurité, la cohérence et l&#39;évolution en étroite relation avec les autres pôles de la Direction du Numérique (DNum). Il contribue à la définition des orientations stratégiques liés au calcul haute performance et aux projets des données de la recherche

Vous trouverez la fiche de poste jointe à ce présent message. Vous pouvez me contacter directement pour plus d’informations.

À bientôt à Strasbourg \!
