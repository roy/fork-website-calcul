Title: Research engineer in Bayesian statistics – Application to ML prediction uncertainty M/F
Date: 2020-03-04 08:27
Slug: job_b9a48d7bb1c71b320b0dc46cf6995985
Category: job
Authors: Adoc Talent Management
Email: apply-60c878bdc36301@adoc-tm.breezy-mail.com
Job_Type: CDI
Tags: cdi
Template: job_offer
Job_Location: Magny-Les-Hameaux
Job_Duration: 
Job_Website: https://en.adoc-tm.com/
Job_Employer: Adoc Talent Management
Expiration_Date: 2020-06-30
Attachment: 

Adoc Talent Management is seeking a Research Engineer specialized in Bayesian statistics, to work on uncertainty quantification for its client, the research centre of a French multinational company active in aeronautical technologies and products.

**Job description**

In the context of a multi-disciplinary project initiated by the research centre on AI use for conception and simulation, you will work on the development of new tools provided by AI, in order to suggest new optimisation strategies for numerical simulations and models representativity.

You will participate in research studies on Predictive Uncertainty Quantification for deep neural networks, involving large-dimensional spaces. You will have for preliminary thinking the recent sampling techniques, together with dimension-reduction methods. This will require a technologic and scientific watch over the project’s life in order to identify the most promising approaches to handle very large-dimension cases.

Finally, you will participate to the internal network of scientific discussion around AI and will valorize the state research with all the departments of the research centre. You will communicate on your results through scientific articles and participation to congresses and international conferences.

**Profile**

You are holding an engineer or PhD diploma and have around 3 years of professional experience on research projects requiring mathematics expertise and statistic tools use, in particular Bayesian methods.

You have an expertise in sampling and high dimension spaces and are very curious to explore other domains and AI tools’ potential.

Through the management of your projects, you could develop a great ability to communicate as well as autonomy and at the same time, a team and sharing spirit.

If you would like to join a French company of international repute and are looking for a position providing both autonomy as well as strong human interactions, don’t hesitate to send us your application (<https://bit.ly/37JQ44p>)
