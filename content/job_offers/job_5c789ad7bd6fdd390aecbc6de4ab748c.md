Title: Deep Learning For Poverty Prediction From Satellite Images
Date: 2020-01-14 09:15
Slug: job_5c789ad7bd6fdd390aecbc6de4ab748c
Category: job
Authors: Laure BERTI
Email: laure.berti@ird.fr
Job_Type: Post-doctorat
Tags: postdoc
Template: job_offer
Job_Location: Montpellier
Job_Duration: 3 years
Job_Website: 
Job_Employer: Foundation for research on biodiversity (FRB)
Expiration_Date: 2020-02-15
Attachment: job_5c789ad7bd6fdd390aecbc6de4ab748c_attachment.pdf

**Your Role**

CESAB Montpellier, France is looking for a postdoc as part of the PARSEC project funded by the Foundation for Research on Biodiversity (FRB) and the Belmont Forum.
The candidate will implement and compare various deep learning architectures for characterizing poverty in the context of land desertification and predict consumption expenditure, using satellite images in various resolutions (e.g. Sentinel 2 sensor with 10 to 60 m resolution and 13 spectral bands, optical SPOT 6/7, and Pleiades images with spatial resolution of 1.5 m and 0.5 m respectively) and other data sources (e.g., the World Bank’s Living Standards Measurement Study surveys, LSMS – <http://surveys.worldbank.org/lsms>), the Demographic and Health Surveys (DHS – <https://dhsprogram.com>), and the Oxford Poverty and Human Development Initiative (OPHI – <https://ophi.org.uk>)).

**Qualifications**

* PhD in Computer Science in the field of deep learning and machine learning
* Knowledge of satellite image analysis is a plus
* Proficiency in Python programming
* Fluent English
* Good communication skills
* Ambition to pursue high-profile and interdisciplinary research
