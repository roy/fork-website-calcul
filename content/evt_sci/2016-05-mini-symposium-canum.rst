CANUM 2016 : mini-symposium "Recherche reproductible"
#####################################################

:date: 2016-05-25 17:00:26
:modified: 2016-05-25 17:00:26
:category: journee
:tags: canum, reproductible
:start_date: 2016-05-13
:end_date: 2016-05-13
:place: Obernai
:summary: Mini-symposium au CANUM 2016 sur la recherche reproductible


.. contents::

.. section:: Description
    :class: description

    La Recherche Reproductible a pour objectif de documenter, archiver et publier une étude scientifique de manière à ce qu'un lecteur d'un article puisse reproduire les résultats présentés. Ces problématiques, ces enjeux concernent tous les domaines et constituent un des fondements de la recherche.
    Lorsque des résultats ont été obtenus par des calculs (sur ordinateur), le lecteur doit pouvoir les re-faire à l'identique pour les reproduire ou avec des modifications pour les analyser. Pour ce faire, il doit avoir accès à tous les éléments permettant d'obtenir ces résultats : données initiales, code source des programmes, compilateur (et les options qui ont été utilisées), ... L'idée est simple, mais sa réalisation nécessite des changements d'habitudes assez profonds. Le calcul numérique est particulièrement délicat dans ce contexte à cause des subtilités de l'arithmétique flottante sur laquelle le développeur n'a pas le contrôle complet dans les environnements de programmation d'aujourd'hui (langages de programmation, compilateur, ...).

    L'objectif de ce mini-symposium est de donner un coup de projecteur sur les enjeux, les difficultés et les solutions en recherche reproductible, et plus particulièrement lorsqu'elle fait intervenir des calculs numériques.

    `Présentation du mini-symposium <attachments/spip/IMG/pdf/recherche_reproductible.pdf>`__

.. section:: Programme
    :class: programme

    .. schedule::

        .. day:: 13-05-2013

            .. event:: Une brève histoire de la recherche reproductible et de ses outils
                :speaker: Christophe Pouzat  (MAP5, Université Paris-Descartes)
                :support: attachments/spip/IMG/html/pouzat_20160513.html
                :begin: 08:30
                :end: 09:00

            .. event:: Arithmétique virgule flottante: la lente convergence vers la rigueur
                :speaker: Jean-Michel Muller (Laboratoire de l'Informatique du Parallélisme)
                :support: attachments/spip/IMG/pdf/muller-canum2016.pdf
                :begin: 09:00
                :end: 09:30

            .. event:: Estimation de la reproductibilité numérique dans les environnements hybrides CPU-GPU
                :speaker: Fabienne Jézéquel (Laboratoire d'Informatique de Paris 6)
                :support: attachments/spip/IMG/pdf/jezequel_canum2016.pdf
                :begin: 09:30
                :end: 10:00

            .. event:: IPOL : Recherche reproductible en traitement d'images
                :speaker: Enric Meinhardt-Llopis (ENS Cachan)
                :support: attachments/spip/IMG/pdf/meinhardt_canum2016.pdf
                :begin: 10:00
                :end: 10:30


.. section:: Organisation
    :class: orga

    - Loic Gouarin
    - Konran Hinsen
    - Florent Langrognet
