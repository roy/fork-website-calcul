Journée "Python et Data Science"
################################

:date: 2017-10-02 16:15:03
:modified: 2017-10-02 16:15:03
:category: journee
:tags: python, data science
:start_date: 2017-12-19
:end_date: 2017-12-19
:place: Rennes
:summary: Journée thématique autour du langage Python et de ses applications en science des données.

.. contents::

.. section:: Description
    :class: description

    Un sondage réalisé par le site `http://www.kdnuggets.com <http://www.kdnuggets.com/2017/08/python-overtakes-r-leader-analytics-data-science.html>`__ sur 954 "data scientists" montre la place de plus en plus importante prise par le langage Python. La majorité des sujets abordés aujourd'hui dans les grandes conférences Python (Scipy, Pycon) concernent aujourd'hui les sciences des données.

    Le groupe calcul avec le soutien du `Centre Henri Lebesgue <https://www.lebesgue.fr>`__ vous propose une journée consacrée au langage Python, à l'écosystème disponible pour tous les "Data scientists" et à des exemples concrets d'utilisation.

    Cette journée a eu lieu le mardi 19 décembre 2017 à Rennes à l'`IRMAR <https://irmar.univ-rennes1.fr>`__.

.. section:: Programme
    :class: programme

    .. schedule::

        .. day:: 19-12-2017

            .. break_event:: Accueil
                :begin: 9:30
                :end: 10:00

            .. event:: Présentation et illustration de l'écosystème Python pour la data science
                :speaker: <a href="https://www.yotta-conseil.fr">Francis Wolinski (Société Yotta Conseil)</a>
                :begin: 10:00
                :end: 10:50
                :support: https://github.com/fran6w/IRMAR/blob/master/IRMAR.pdf

            .. event:: Présentation des différents concepts du parallélisme et du paysage des librairies de calcul distribué/parallèle en Python
                :speaker: <a href="https://www.linkedin.com/in/gaël-pegliasco-11511558/?ppe=1"> Gaël Pegliasco (Makina Corpus)</a>
                :begin: 10:50
                :end: 11:40
                :support: https://github.com/MordicusEtCubitus/PresentationCalculDistribuePython


            .. event:: L codent, L créent ou comment démystifier la programmation auprès des jeunes filles ?
                :speaker: <a href="http://www.lifl.fr/~pupin/"> Maude Pupin (Université Lille 1)</a>
                :begin: 11:40
                :end: 12:30

            .. break_event:: Repas
                :begin: 12:30
                :end: 14:00

            .. event:: TensorFlow et les réseaux de neurones
                :speaker: <a href="http://www-irma.u-strasbg.fr/~vigon/">Vincent Vigon (Université de Strasbourg)</a>
                :begin: 14:00
                :end: 14:50
                :support: http://prezi.com/xsxa9d3ujuji/?utm_campaign=share&utm_medium=copy&rc=ex0share

            .. event:: Lutter contre le paludisme avec Python
                :speaker: <a href="http://cupnet.net/about/">Pierre Poulain (Université Paris 7)</a>
                :begin: 14:50
                :end: 15:40

            .. event:: Job Offer Skills Analysis
                :speaker: <a href="http://ghostweather.com">Lynn Cherny (EMLYON Business School)</a>
                :begin: 15:40
                :end: 16:30

            .. event:: Discussions
                :begin: 16:30
                :end: 17:00

.. section:: Organisation
    :class: orga

    - Pierre Navaro (IRMAR Rennes)
