JCAD, Journées Calcul & Données
###############################

:date: 2018-06-22 15:27:31
:modified: 2018-06-22 15:27:31
:category: journee
:tags: jcad
:start_date: 2018-10-24
:end_date: 2018-10-26
:attendees: 100
:place: Lyon
:summary: Les GIS FRANCE GRILLES et GRID'5000, le Groupe Calcul, le GDR RSD, GENCI et les partenaires de l'Equipex EQUIP@MESO organisent ensemble les JCAD, Journées Calcul Données.

.. contents::

.. section:: Description
    :class: description

    Les 11èmes journées mésocentres auront un format particulier cette année.

    Les GIS FRANCE GRILLES et GRID'5000, le Groupe Calcul, le GdR RSD, GENCI et les partenaires de l'Equipex EQUIP@MESO organisent ensemble les JCAD, Journées Calcul & Données : rencontres scientifiques et techniques du calcul et des données. Ces journées rassemblent à la fois les objectifs, les thématiques ainsi que les publics cibles des journées Mésocentre (Groupe Calcul), des journées Mésochallenge (Equipex EQUIP@MESO) et des journées SUCCES (France Grilles, Grid'5000, GDR RSD, Groupe Calcul) et en prennent le relai. Les JCAD sont donc dédiées à la fois aux utilisateurs et aux experts techniques des infrastructures et des services associés.

    Ces journées auront lieu à `l'ENS Lyon <http://www.ens-lyon.fr/>`__, du 24 au 26 octobre 2018.

    Pages des journées : https://jcad2018.sciencesconf.org/
