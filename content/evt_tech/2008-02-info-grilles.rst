Réunion d'information et de concertation à propos des grilles de calcul
#######################################################################

:date: 2008-02-14 14:30:18
:start_date: 2008-02-14
:end_date: 2008-02-14
:category: journee
:tags: grille
:place: Institut Henri Poincaré


.. contents::

.. section:: Description
   :class: description

   Réunion d'information et de concertation à propos des grilles de calcul organisée par la `CPU <http://www.cpu.fr>`__.

   Les moyens informatiques sont un enjeu stratégique chaque jour plus important pour la recherche dans la plupart des domaines et constituent un continuum d'équipements depuis les moyens de laboratoires, les "mésocentres" (régionaux ou thématiques) jusqu’aux très grands centres de calcul. Une coordination entre ces différents niveaux est naturellement nécessaire. En particulier, les "grilles" de calcul sont un outil du partage de ces ressources.

   Les universités jouent naturellement un rôle essentiel dans ce dispositif et elles se doivent d'y jouer un rôle moteur. A l'initiative du ministère,  les universités, à travers la `CPU <http://www.cpu.fr/>`__ ont été associés à plusieurs opérations dans ces domaines  qui nécessitent naturellement une concertation avec les  organismes de recherche impliqués  (CNRS, CEA, INRIA, INRA, INSERM...). En ce qui concerne les très grands équipements, cela a conduit à la création du Grand Equipement National pour le Calcul Intensif  (GENCI, société civile) qui a pour but de coordonner  les moyens de calculs existants et les projets futurs.   En ce qui concerne les grilles, le ministère a mis en place en place deux groupes de prospectives qui doivent faire des propositions pour la rentrée 2008 (en fait pour novembre) : l'un concerne les grilles de recherche et intéresse plus particulièrement la communauté informatique. Il s'appuie notamment sur l'expérience de l'INRIA à travers le projet GRID 5000; l'autre est destiné aux « grilles de production » et nécessite une concertation large de l'ensemble des acteurs des domaines susceptibles d'utiliser à court ou moyen terme ce type d'infrastructure.

   Dans ce cadre du groupe de travail sur les grilles de production, la Conférence des présidents d'Université (`CPU <http://www.cpu.fr/>`__) ont organisé une réunion d'informations et de concertation qui a été l'occasion de faire un premier recensement des acteurs, des différents moyens mobilisables et des besoins et également de définir les modalités et les objectifs d'une enquête en ligne auprès de l'ensemble des collègues concernés.

   Cette réunion a été couplée avec la `journée "mésocentre", organisée par le groupe calcul <2008-02-journee-mesocentres.html>`__ qui a lieu la veille au même endroit. Les comptes rendu de ces deux journées est `en ligne <attachments/spip/Documents/Journees/fev2008/CR-13_14-02.pdf>`__.

.. section:: Date et lieu
    :class: description

    Le jeudi 14 février 2008.

    `Institut Henri Poincaré <http://www.ihp.fr/>`__, 11 rue Pierre et Marie Curie - 75231 Paris Cedex 05.

.. section:: Programme
   :class: programme

      .. schedule::

	 .. day:: 14-02-2008

	    .. event:: Présentations et discussions
	       :begin: 10:00
	       :end: 12:00
	       :support:
		  [O. Pironneau](attachments/spip/Documents/Manifestations/CPU2008/CSCIpresente080214.pdf)
		  [D. Boutigny](attachments/spip/Documents/Manifestations/CPU2008/IdG.pdf)
		  [F. Capello & T. Priol](attachments/spip/Documents/Manifestations/CPU2008/ALADDIN-CPU.pdf)
		  [M. Kern](attachments/spip/Documents/Manifestations/CPU2008/Journee_CPU.pdf)
		  [V. Louvet & F. Berthoud](attachments/spip/Documents/Manifestations/CPU2008/Calcul-mesocentres-14-02.pdf)
		  [S. Cordier & L. Desbat](attachments/spip/Documents/Manifestations/CPU2008/SCetLD_JMesoCentre140208.pdf)

	       Dans une première partie, nous aurons quelques brèves présentations :

               - Olivier Pironneau (Président du Comité Stratégique pour le Calcul Intensif)
	       - Dominique Boutigny ( Institut Des Grilles )
	       - Franck Cappello et Thierry Priol (Grid'5000-ALADDIN)
	       - Michel Kern (DGRI - Ministère)
	       - Violaine Louvet, Françoise Berthoud (Groupe Calcul)
	       - Le rapport sur les mésocentres ayant servi de base à cette présentation est en ligne .
	       - Stéphane Cordier et Laurent Desbat : proposition d'enquête sur les grilles

	       La seconde partie a été l'occasion d'échanges avec la salle, la CPU ayant proposé à tous les  établissements souhaitant contribuer à la réflexion dans ce domaine, d'envoyer un représentant.

               Liste provisoire des établissements ayant indiqué leur représentant :

               - ENS de Lyon
	       - INP Toulouse
	       - INSA de Rouen
	       - INSA de Toulouse
	       - Observatoire de Paris
	       - Université Blaise Pascal (Clermont 2)
	       - Université de Bordeaux I
	       - Université de Bretagne Sud
	       - Université de Cergy-Pontoise
	       - Université Claude Bernard (Lyon 1)
	       - Université de Franche-Comté
	       - Université de Grenoble
	       - Université de Lille 1
	       - Université de Limoges
	       - Université Louis Pasteur (Strasbourg)
	       - Université Lumière Lyon 2
	       - Université d'Orléans
	       - Université de la Méditerranée (Aix-Marseille 2)
	       - Université Paris Descartes (Paris 5)
	       - Université Paris Diderot (Paris 7)
	       - Université Paul Sabatier (Toulouse 3)
	       - Université de Picardie Jules Verne (Amiens)
	       - Université Pierre et Marie Curie (Paris 6)
	       - Université de Reims Champagne-Ardenne
	       - Université de Savoie (Chambéry)
	       - Université de Technologie de Troyes
	       - Université de Valenciennes et du Hainaut Cambrèsis (UVHC)

.. section:: Organisation
   :class: orga

   - Stéphane  Cordier 
   - Laurent Desbat
