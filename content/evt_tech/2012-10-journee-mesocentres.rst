5ème journées mésocentres
#########################

:date: 2012-06-27 17:24:25
:category: journee
:authors: Romaric David
:place: Paris
:start_date: 2012-10-01
:end_date: 2012-10-03

.. contents::

.. section:: Description
  :class: description

  Le GIS France Grilles et le Groupe Calcul organisent pour la première fois en coordination les rencontres scientifiques et les journées annuelles des mésocentres à l'Institut de Physique du Globe de Paris, 1, rue Jussieu, 75005 Paris, du 1er au 3 octobre 2012.

  Les supports sont en ligne : http://mesogrilles2012.sciencesconf.org/resource/page/id/12

  L’objectif de ces rencontres est double :

    - réaliser un bilan d’étape des évolutions marquantes dans le paysage des mésocentres d’une part, des infrastructures de grilles et de cloud d’autre part.
    - présenter des travaux scientifiques, dans toutes les disciplines, réalisés grâce au soutien des mésocentres, des infrastructures de grilles de calcul ou de cloud.

  Le format est innovant cette année à deux titres :

    - la colocalisation des journées mésocentres et des rencontres scientifiques France-Grilles
    - un appel à propositions lancé dans les deux communautés qui a permis de sélectionner les présentations en séance plénière et les posters. Le meilleur poster sera récompensé par un prix.

    Les journées sont financées par France Grilles, le groupe calcul et GENCI avec le soutien de la Conférence des Présidents d’Université.

    La totalité de l'événement sera retransmis en webcast.

.. section:: Programme
  :class: programme

  .. schedule::

    .. day:: 01-10-2012

      .. event:: Ouverture:
        :begin: 13:50
        :end: 14:00
        :speaker: Romaric David et Geneviève Romier

      .. event:: Reproductive strategies, demography and mutational meltdown,
        :begin: 14:00
        :end: 14:30
        :speaker: Diala Abu Awad, laboratoire GEPV - Université Lille 1

      .. event:: Efficient fault monitoring with Collaborative Prediction,
        :begin: 14:00
        :end: 14:30
        :speaker: Cécile Germain, Laboratoire de Recherche en Informatique (LRI)

      .. event:: Efficient fault monitoring with Collaborative Prediction,
        :begin: 14:00
        :end: 14:30
        :speaker: Cécile Germain, Laboratoire de Recherche en Informatique (LRI)

      .. event:: 5 années de mutualisation des ressources en calcul scientifique au PSMN de l'ENS-Lyon
        :begin: 14:30
        :end: 15:00

      .. event:: GINSENG (Global Initiative for Sentinel E-health Network on Grid)
        :begin: 15:00
        :end: 15:30

      .. event:: MSDA / viGrid : La grille de calcul EGI au service de l'analyse protéomique,
        :begin: 16:00
        :end: 16:30
        :speaker: Christine Carapito, Institut Pluridisciplinaire Hubert Curien  (IPHC)

      .. event:: Synergie entre le mesocentre Grenoblois CIMENT et la grille europeene EGI pour la recherche de nouvelles particules dans l'experience ATLAS aupres du collisionneur LHC au CERN.
        :begin: 16:30
        :end: 17:00

    .. day:: 02-10-2012

      .. event:: Ouverture, présentation du rapport annuel mésocentres
        :begin: 09:00
        :end: 09:30

      .. event:: Genci + Equip@Meso,
        :begin: 09:30
        :end: 10:00
        :speaker: Catherine Rivière

      .. event:: La parole aux nouveaux (1) : présentation du mésocentre de l'ECP,
        :begin: 10:00
        :end: 10:30
        :speaker: Anne-Sophie Mouronval, Laurent Series,  Matthieu Boileau

      .. event:: La parole aux nouveaux (2) : présentation du projet de mésocentre lorrain, montage du projet et GreenIT,
        :begin: 11:00
        :end: 11:30
        :speaker: Isabelle Charpentier, Bernard Dussoubs

      .. event:: Retour sur journée mésocentres 2011 : 1 an de mésocentre aquitain,
        :begin: 11:30
        :end: 12:00
        :speaker: Pierre Gay

      .. event:: Présentation France-Grilles
        :begin: 12:00
        :end: 12:30
        :speaker: Geneviève Romier

      .. event:: Data-intensive challenges in seismology: an overview of the FP7-INFRASTRUCTURE-2011-2 project VERCE"
        :begin: 14:30
        :end: 15:00
        :speaker: Jean-Pierre Vilotte, IPGP

      .. event:: Biodiversité orateur
        :begin: 15:00
        :end: 15:30
        :speaker: Alain Franc, INRA

      .. event:: Exposé Big Data
        :begin: 15:30
        :end: 16:00
        :speaker: Jacques-Charles Lafoucrière, CEA

      .. event:: Exposé Big Data
        :begin: 16:00
        :end: 16:30
        :speaker: Frédéric Desprez, Frédéric Sutter

      .. event:: Table Ronde "quel avenir pour le cloud dans le HPC et sur les grilles ?"
        :begin: 16:30
        :end: 18:00

    .. day:: 03-10-2012

      .. event:: A science-gateway workload archive application to the self-healing of workflow incidents"
        :begin: 09:00
        :end: 09:30

      .. event:: SysFera-DS : Un portail d'accès unifié aux ressources des centres de calcul. Mise en application à EDF R&D
        :begin: 09:30
        :end: 10:00

      .. event:: Algorithmes évolutionnaires sur grille de calcul pour le calibrage de modéles géographiques
        :begin: 10:30
        :end: 11:00

      .. event:: GeoQAIR : Quantification de l'apport d'une plateforme d'observations Géostationnaires pour la surveillance de la Qualité de l'AIR en Europe,
        :begin: 11:00
        :end: 11:30
        :speaker: Gaëlle Dufour, Laboratoire Interuniversitaire des Systèmes Atmosphériques (LISA)

      .. event:: Diffusion MRI Simulation with the Virtual Imaging Platform
        :begin: 11:30
        :end: 12:00

      .. event:: Hadoopizer : a cloud environment for bio-informatics data analysis,
        :begin: 12:00
        :end: 12:30
        :speaker: Anthony Bretaudeau,  Plateforme GenOuest INRIA/Irisa
