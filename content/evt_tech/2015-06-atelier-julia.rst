Atelier Julia
#############

:date: 2015-04-30 10:32:24
:category: formation
:start_date: 2015-06-16
:end_date: 2015-06-17
:place: Orsay
:tags: julia
:summary: Atelier sur le langage Julia


.. contents::

.. section:: Description
    :class: description

    Le groupe Calcul a proposé un atelier sur le langage `Julia <http://julialang.org/>`__ les **16 et 17 juin 2015**  sur le campus d'Orsay au LAL.

    Cette formation a été donnée par `Dr. David P. Sanders <http://sistemas.fciencias.unam.mx/~dsanders>`__ (Departament of Physics, Faculty of Sciences, Universidad Nacional Autónoma de México (`UNAM <http://www.unam.mx>`__)).

    L'unique prérequis pour ce cours était d’avoir des notions de programmation (boucles, variables, ...).

    Les supports de la formation se trouvent `ici <https://github.com/dpsanders/hands_on_julia>`__.


.. section:: Résumé
    :class: description

    Hands-on Julia for scientific computing

    This will be a tutorial workshop on the `Julia <http://julialang.org/>`__ language, consisting mainly of hands-on exercises, designed to take participants who are experienced scientific programmers, but who have no knowledge of Julia, to a stage where they are beginning to feel comfortable with Julia and are able to write fairly idiomatic Julia code.

    The emphasis will be on those features that *differ* from other well-known languages used for scientific computing (e.g. Fortran, C, C++, Python, R, and MATLAB).

    The current stable version of Julia is the 0.3 line (0.3.7 is the current latest release).
    There are several important changes in 0.4, which will be in the process of being finalised when the workshop takes place; we will try to highlight the differences.


.. section:: Programme
    :class: programme

        .. schedule::

            .. day:: 16-06-2015

                .. event:: Basic Julia
                    :begin: 09:00
                    :end: 12:00
                    :speaker: David P. Sanders

                    - **Introduction**

                        - Why should I learn Julia?
                        - How can I use Julia?

                            - REPL (terminal), IJulia (notebook), Juno (IDE), editors
                            - JuliaBox (and SageMathCloud?)

                    - **Basic Julia**

                        - Numbers and types
                        - Vectors, matrices and arrays
                        - Conditionals and loops
                        - Some curious syntax features

                            - Short-circuit evaluation

                    - **Plotting**

                        - Basic plotting (PyPlot and Gadfly)
                        - Interactive widgets in IJulia

                    - **Functions**

                        - To type or not to type
                        - How functions work in Julia: methods and multiple dispatch

                            - Why this is at the core of the Julia experience

                    - **Composite types**

                        - Julia's take on object-oriented programming
                        - Conversion and promotion
                        - Multiple dispatch again

                .. event:: Getting a feel for Julia
                    :begin: 14:00
                    :end: 18:00
                    :speaker: David P. Sanders

                    - **Structuring our Julia code**

                        - Scripts and command-line arguments
                        - Modules
                        - `include`, `import` and `using`
                        - Documentation
                        - Stand-alone Julia executable (0.4)

                    - **Debugging**

                        - Error handling
                        - Testing
                        - Debugging

                    - **Towards good (=C) performance**

                        - No globals, please
                        - Type stability
                        - Profiling
                        - How to check allocation


            .. day:: 17-06-2015

                .. event:: Advanced concepts
                    :begin: 09:00
                    :end: 12:00
                    :speaker: David P. Sanders

                    - Some Julia internals
                    - Metaprogramming:

                        - Code that writes code
                        - Macros

                    - From 0.3 to 0.4


                .. event:: Let's apply it
                    :begin: 14:00
                    :end: 18:00
                    :speaker: David P. Sanders

                    - Developing a Julia package and contributing to Julia itself
                    - Write some code in Julia! Use pair programming
                    - Where do I go from here?
