Utilisation de Grid'5000 pour la réalisation de benchmarks
##########################################################

:date: 2020-10-08 09:00:00
:category: formation
:tags: grid5000, tests, benchmarks
:start_date: 2020-10-08
:end_date: 2020-10-08
:place: Lyon
:summary: Formation à l'usage de la plateforme Grid'5000 pour l'expérimentation numérique et des benchmarks
:inscription_link: https://indico.mathrice.fr/event/213/registration

.. contents::

.. section:: Description
    :class: description
    

    Cette formation à distance propose de montrer l'usage de Grid'5000 comme plateforme d'expérimentation pour les personnels participant à l'administration de machines de calcul. Il s'agit d'apprendre à utiliser la plateforme pour leurs tests de déploiement et le benchmark de différents matériels et logiciels.

    Grid'5000 est une infrastructure de recherche en informatique distribuée (incluant notamment les domaines des systèmes distribués, du Cloud, des réseaux, du calcul haute performance…) qui permet la réalisation d'expériences en mettant à disposition plus de 800 noeuds groupés en une trentaine de clusters pouvant être réservés et entièrement reconfigurés.

    La journée comportera une partie de cours pour prendre en main la plateforme, suivie de travaux pratiques proposant deux exemples d'expérimentation à manipuler par les participants.

    **Prérequis**
    
    - Être autonome en environnement Linux / shell bash
    - Avoir une expérience de test de matériel pour l'administration de clusters de calcul ou une expérience de réaisation de benchmarks

    Date limite d'inscription : **07/10/2020**

.. section:: Programme
    :class: programme

    .. schedule::
        :indico_url: https://indico.mathrice.fr/
        :indico_event: 213

.. section:: Organisation
    :class: orga

        - Simon Delamare
        - Laurent Pouilloux
        - Anne Cadiou


