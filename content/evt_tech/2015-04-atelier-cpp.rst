Atelier C++: les bases du 11 et du 14
#####################################

:date: 2015-02-18
:category: formation
:tags: c++
:authors: Loïc Gouarin
:place: Paris
:start_date: 2015-04-28
:end_date: 2015-04-30
:attendees: 40

.. contents::

.. section:: Description
  :class: description

    Le groupe Calcul propose une formation pour débutant en programmation C++11
    et 14 les 28, 29 et 30 avril 2015  dans l'amphi Hermite  à
    l'IHP  (http://www.ihp.fr/).

    Cette formation sera donnée par
    `Joël Falcou <https://www.lri.fr/~falcou/>`__ et
    `Serge Guelton <http://serge.liyun.free.fr/serge/index.html>`__.

    L'unique prérequis pour ce cours est d'avoir des notions de programmation
    (boucles, variables, ...).

    Pour suivre correctement cette formation, les participants devront se munir
    d'un ordinateur portable avec g++ 4.8+ ou clang 3.4+.

    Les supports de l'atelier :

      - `formation_base_cpp.zip <attachments/spip/IMG/zip/formation_base_cpp.zip>`__
      - https://github.com/serge-sans-paille/land_of_cxx

.. section:: Programme
    :class: programme

    .. schedule::

      .. day:: 28-04-2015

        .. event:: Jour 1
          :begin: 10:00
          :end: 17:00
          :speaker: Joel Falcou et Serge Guelton


          - Introduction

            - Historique du langage
            - Pourquoi C++ ?
            - L’héritage du C
            - Normes et évolutions

          - Structures de base d’un programme C++

            - Types et variables
            - Structures de controle
            - Fonctions et procédures
            - Processus de compilation
            - Mise en pratique

          - Aspects impératif

            - Définir une fonction
            - Paramètres, arguments et valeurs de retour
            - Inférence de type
            - Gestion des erreurs
            - Mise en pratique

      .. day:: 29-04-2015

        .. event:: Jour 2
          :begin: 09:00
          :end: 17:00
          :speaker: Joel Falcou et Serge Guelton

          - Entrées/Sorties

            - Notions de flux
            - Entrées et sorties standards
            - Fichiers textes et binaires
            - Mise en pratique

          - La bibliothèque standard

            - Conteneurs
            - Algorithme
            - Fonctions mathématiques
            - Dates et heures
            - Expressions régulières
            - Mise en pratique

      .. day:: 30-04-2015

        .. event:: Jour 3
          :begin: 09:00
          :end: 17:00
          :speaker: Joel Falcou et Serge Guelton

          - Programmation orientée objets

            - Principes généraux
            - Notion d’interface
            - Héritage
            - Principes de substitution de Liskov
            - Mise en pratique

          - Gestion des ressources systèmes

            - Principe de la RAII
            - Sémantique de valeur, sémantique d’entité
            - Pointeurs à sémantique riche
            - Mise en pratique

          - Programmation générique

            - Fonctions génériques
            - Structures génériques
            - Mise en pratique
