Journée Évaluation de performance
#################################

:date: 2018-09-17 17:32:30
:category: journee
:place: Paris
:start_date: 2018-10-08
:end_date: 2018-10-08

.. contents::

.. section:: Description
    :class: description

        Le réseau Calcul et le mésocentre de l'Université PSL organisent le lundi 8 octobre 2018 une journée d'introduction à l'évaluation de performance des codes de simulation. Cette journée aura lieu à l'Observatoire de Paris.

        Les supports des présentations sont disponibles sur `la page Indico de la journée <https://indico.obspm.fr/event/60/>`_.


.. section:: Programme
    :class: programme

    .. schedule::

        .. day:: 08-10-2018

            .. event:: Architecture des calculateurs
                :speaker: Matthieu Haefele
                :begin: 9:00
                :end: 9:45

            .. event:: Méthodologie et métriques pour l'analyse de performance
                :speaker: Mathieu Lobet
                :begin: 9:45
                :end: 10:15

            .. break_event:: Pause
                :begin: 10:15
                :end: 10:45

            .. event:: Présentation scalasca
                :speaker: Matthieu Haefele
                :begin: 10:45
                :end: 11:15

            .. event:: TP scalasca
                :speaker: Matthieu Haefele
                :begin: 11:15
                :end: 13:00


            .. break_event:: Déjeuner
                :begin: 13:00
                :end: 14:00

            .. event:: Quelques notions optimisation à l'échelle du coeur de calcul
                :speaker: Matthieu Haefele
                :begin: 14:00
                :end: 14:30

            .. event:: Présentation VTune / Advisor
                :speaker: Mathieu Lobet
                :begin: 14:30
                :end: 15:30

            .. event:: Pause
                :begin: 15:30
                :end: 16:00

            .. event:: TP VTune / Advisor
                :speaker: Mathieu Lobet
                :begin: 16:00
                :end: 17:30

.. section:: Organisation
    :class: orga

        - Fabrice Roy, LUTH, CNRS
        - Matthieu Haefele, Maison de la Simulation
        - Mathieu Lobet, Maison de la Simulation

.. section:: Partenariat
    :class: orga

        - MesoPSL, mésocentre de l'Université PSL
