ANF - Informatique pour le calcul scientifique : bases et outils
################################################################

:date: 2014-03-28 21:21:54
:category: formation
:authors: Romaric David
:place: Saint-Dié
:start_date: 2014-09-22
:end_date: 2014-09-26

.. contents::

.. section:: Lieu et inscriptions
  :class: description

  Cette école a eu lieu du Lundi 22 au Vendredi 26 Septembre 2014 dans les Vosges, au Village Vacances Cap France La Bolle (près de Saint-Dié - 88).

  Il n'y avait pas de frais d'inscription pour cette école. L'hébergement est compris. Restent à la charge des participants les frais de transport aller et retour, ainsi que les extras sur place.

  Vous deviez disposer d'un ordinateur portable pour suivre cette école.

.. section:: Programme 
  :class: programme

  .. schedule::

    .. day:: 22-09-2014

      .. event:: Introduction générale
        :begin: 14:00
        :end: 15:00
        :speaker: Thierry Dumont
        :support:
          attachments/spip/IMG/pdf/aa.pdf
          attachments/spip/IMG/pdf/cc.pdf
          attachments/spip/IMG/bin/float.bin

        Le but est de décrire des notions de bases qu'il convient de connaitre pour le développement de codes scientifiques comme les architectures des machines, le codages des nombres ...

      .. event:: Installation des machines virtuelles et rappel des commandes de bases de Linux (si besoin),
        :begin: 15:00
        :end: 16:00
        :speaker: Anne-Sophie Mouronval et Laurent Series


    .. day:: 23-09-2014

      .. event:: Journée consacrée à la compilation et au débogage
        :begin: 09:00
        :end: 17:00
        :speaker: Patrick Carribault
        :support:
          attachments/spip/IMG/pdf/TP_Compilation_Correction.pdf
          attachments/spip/IMG/tgz/TP_COMPIL_CORRECTION.tgz

        - les différences entre un langage compilé et interprété
        - les différentes étapes de compilation (compilation, édition de lien)
        - les principales options de compilation permettant notamment d'optimiser le code
        - la compilation séparée et sa mise en pratique avec les Makefiles
        - le débogage et sa mise en pratique avec gdb

    .. day:: 24-09-2014

      .. event:: Bibliothèques mathématiques
        :begin: 09:00
        :end: 12:00
        :speaker: Jérémie Gaidamour
        :support:
          attachments/spip/IMG/pdf/Bibliotheques_Mathematiques.pdf
          attachments/spip/IMG/bin/TP.bin

        - les principales bibliothèques scientifiques
        - leurs intérêts : réutilisabilté, performance, fiabilité ...
        - mise en pratique


      .. event:: Visite d'une confiserie artisanale puis de la ville de Gérardmer.
        :begin: 14:00
        :end: 17:00


      .. event:: Introduction aux outils de gestion de versions et première mise en pratique sur git.
        :begin: 18:00
        :end: 19:00
        :speaker: Lucas Nussbaum
        :support:
          attachments/spip/IMG/pdf/git-slides.pdf
          attachments/spip/IMG/zip/Git.zip

    .. day:: 25-09-2014

      .. event:: Scilab, Matlab.
        :begin: 09:00
        :end: 12:00
        :speaker: Bruno Pinçon

          - découvrir des environnements (ou boîtes à outils) de calcul intégré (calcul formel, calcul matriciel)
          - mettre en pratique sur des environnements issus du logiciel libre (Scilab)
          - comprendre comment sont construites ces boites a outils

      .. event:: Intro Sage, E/S et visualisation.
        :begin: 14:00
        :end: 16:00
        :speaker: Pierre Navaro et Romaric David
        :support:
             attachments/spip/IMG/pdf/sagemath-anf.pdf
             attachments/spip/IMG/pdf/2014_09_io.pdf

          - Un petit mot sur sage, une autre boîte à outils numérique.
          - les différents types E/S (formaté ou binaire)
          - l'impact des E/S et l'importance qui doit être accordée à leur traitement
          - les bibliothèques existantes

    .. day:: 26-09-2014

      .. event:: Introduction au parallélisme.
        :begin: 09:00
        :end: 12:00
        :speaker: Gérald Monard
        :support:
            attachments/spip/IMG/zip/Parallele.zip

          - les principales architectures parallèles
          - les principaux modèles de programmation parallèle


.. section:: Organisation
  :class: orga

  - Anne-Sophie Mouronval
  - Laurent Series
  - Bernard Dussoubs
  - Romaric David
