Paysage du |_| Calcul
#####################

:date: 2014-04-17
:modified: 2018-12-03
:slug: paysage_du_calcul

.. contents::

.. section:: Mésocentres
    :class: description

    - Ressources de calculs et de conseils de proximité pour les scientifiques
    - Ordre de grandeur des allocations de temps de calcul : 1k-1M heures coeur
    - Le Groupe Calcul, en lien avec Genci_, participe à la coordination et l'animation des
      personnels en charge de ces infrastructures.

    .. button:: En savoir plus
        :target: mesocentres_en_france.html

.. section:: Centres nationaux
    :class: description

    - Ressources de calculs pour la production de résultat à grande échelle
    - Ordre de grandeur des allocations de temps de calcul : 500k-10M heures coeur
    - Trois centres nationaux gérés par Genci_ dont les demandes doivent être effectuer via le `DARI <https://www.edari.fr/>`_ :

        - l'IDRIS_
        - le CINES_
        - le TGCC_

    - Le `CC-IN2P3 <https://cc.in2p3.fr/>`_ plus spécialisé dans la physique des particules, la physique nucléaire et la physique des astroparticules

.. section:: Ressources européennes
    :class: description

    - Jusqu'en 2021, ce sont les états nations de l'UE qui achètent les supercalculateurs avec une subvention de l'UE. En contre-partie une partie des heures de calcul sont reversées à `PRACE <http://www.prace-ri.eu/>`_ qui les octroient aux chercheurs de l'UE qui en font la demande lors d'`appels à projet <http://www.prace-ri.eu/call-announcements/>`_.
    - Ordre de grandeur des allocations de temps de calcul : 5M-100M heures coeur
    - Cela permet d'accèder aux `7 calculateurs les plus puissants d'Europe <http://www.prace-ri.eu/prace-resources/>`_
