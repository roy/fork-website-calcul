"use strict";

function read(data){
  return data;
}


function parse_event(data, nb_event,where){
  var element = "indico";
  var output = '';

  var indico_event = data.results[0];

  var titre =document.getElementsByTagName("h2");
  titre[0].innerHTML = indico_event.title;
  output += '<h1>' + indico_event.title + '</h1>';
  output += '<h1>' + indico_event.location + '</h1>';
  output += '<h1>' + indico_event.address + '</h1>';
  output += '<h1>' + indico_event.room + '</h1>';
  output += '<h1>' + indico_event.description + '</h1>';

  document.getElementById(where).innerHTML += output;

}



function read_indico_timetable(url, nb_event)
{

  $.ajax( {
    type: "GET",
    url: url + "/export/timetable/" + nb_event + ".jsonp?callback=read?",
    dataType: "jsonp",
    jsonp: false,
    jsonpCallback: "read",
    success: function(jsonp) {
    },
    error: function(event) {
      alert("unable to read the event " + nb_event + " from " + url);
    },
    complete: function(data){
      IndicoPlanning.parse(data.responseJSON, nb_event,'indico');
    }

  });
}


function read_indico_event(url, nb_event)
{
  $.ajax( {
    type: "GET",
    url: url + "/export/event/" + nb_event + ".jsonp?callback=read?",
    dataType: "jsonp",
    jsonp: false,
    jsonpCallback: "read",
    success: function(jsonp) {
      read_indico_timetable(url,nb_event);
    },
    error: function(event) {
      alert("unable to read the event " + nb_event + " from " + url + "/export/event/" + nb_event + ".jsonp?callback=read?");
    },
    complete: function(data){
      parse_event(data.responseJSON, nb_event,'indico');
    }

  });
}


var url = $('#indico').html().trim(),
    event = $('#indico_event').html().trim();

read_indico_event(url,event);

$('#indico').show();
